import 'package:flutter/material.dart';
import 'package:codestatsapp/utils/api_service.dart';
import 'package:codestatsapp/ui/styles.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:percent_indicator/percent_indicator.dart';

class CodestatsApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'CodeStats', theme: ThemeData.dark(), home: HomePage(title: 'Virginia_Sanchez'));
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Mi perfil", style: h5()),
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Colors.transparent,
          actions: [CircleAvatar(backgroundImage: imagenPerfil(), radius: 27)],
          elevation: 0.0),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            // Banner de información
            FutureBuilder<ApiCodeStats>(
              future: getData(widget.title),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return UserAccountsDrawerHeader(
                      accountName: Text("${widget.title}, [${getLevel(snapshot.data.totalXp)}]", style: h2()),
                      accountEmail: Text(
                          "Nueva: ${snapshot.data.newXp} | Total: ${snapshot.data.totalXp}\n"
                          "Ultima vez programando: ${snapshot.data.dates.keys.toList()[(snapshot.data.dates.length) - 1]}",
                          style: h6()),
                      decoration: BoxDecoration(image: DecorationImage(image: imagenBanner(), fit: BoxFit.cover)));
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),

            // Lista de ítems
            ListTile(title: Text("Inicio"), onTap: () {}),
            ListTile(title: Text("Experiencia Semanal"), onTap: () {}),
            ListTile(title: Text("Experiencia Mensual"), onTap: () {}),
          ],
        ),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              // Hola, usuario
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  //TODO: [MAIN] Agregar un InputText para buscar usuarios
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text("Hola, ${widget.title}", style: h3()),
                  ),
                ],
              ),

              // Experiencia por lenguaje
              Card(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text("Experiencia por lenguaje", style: h5(), textAlign: TextAlign.left),
                        ),
                      ],
                    ),
                    FutureBuilder<ApiCodeStats>(
                      future: getData(widget.title),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Container(
                            height: MediaQuery.of(context).size.height * 0.50,
                            child: ListView.builder(
                              itemCount: snapshot.data.languages.keys.toList().length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, int index) {
                                return CardLanguage(
                                  lenguaje: snapshot.data.languages.keys.toList()[index],
                                  experienciaTotal: snapshot.data.languages[snapshot.data.languages.keys.toList()[index]].xps,
                                  nuevaExperiencia: snapshot.data.languages[snapshot.data.languages.keys.toList()[index]].newXps,
                                  color: Colors.black,
                                );
                              },
                            ),
                          );
                        } else {
                          return CircularProgressIndicator();
                        }
                      },
                    ),
                  ],
                ),
              ),

              // Experiencia por fechas
              Card(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text("Experiencia por fechas", style: h6()),
                        ),
                      ],
                    ),
                    FutureBuilder<ApiCodeStats>(
                      future: getData(widget.title),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          List<double> xpDias = [];
                          //TODO: Ordenar por fecha
                          for (var i in snapshot.data.dates.values.toList()) {
                            xpDias.add(i.toDouble());
                          }
                          return Container(
                            margin: EdgeInsets.all(5),
                            child: Sparkline(
                              //TODO: [API] Ordenar la lista de fechas PREVIAMENTE al llamado. Esto es en Api_Service.dart
                              data: xpDias,
                              fillMode: FillMode.below,
                              fillGradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Colors.blue[400],
                                  Colors.blueAccent[400],
                                ],
                              ),
                            ),
                          );
                        } else {
                          return CircularProgressIndicator();
                        }
                      },
                    ),
                  ],
                ),
              ),

              // Experiencia por máquinas
              Card(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text("Experiencia por Máquinas", style: h6()),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FutureBuilder<ApiCodeStats>(
                            future: getData(widget.title),
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                return Expanded(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      CircularExperience(
                                          encabezado: Text(
                                              "${snapshot.data.machines.keys.toList()[0]}\n"
                                              "Nivel: ${getLevel(snapshot.data.machines[snapshot.data.machines.keys.toList()[0]].xps)}",
                                              style: h5(),
                                              textAlign: TextAlign.center),
                                          dato: snapshot.data.machines[snapshot.data.machines.keys.toList()[0]].xps),
                                    ],
                                  ),
                                );
                              } else {
                                return CircularProgressIndicator();
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(
            () {
              // This call to setState tells the Flutter framework that something has
              // changed in this State, which causes it to rerun the build method below
              // so that the display can reflect the updated values. If we changed
              // _counter without calling setState(), then the build method would not be
              // called again, and so nothing would appear to happen.
            },
          );
        },
        tooltip: 'Actualizar',
        child: Icon(Icons.refresh),
      ),
    );
  }
}

class CircularExperience extends StatelessWidget {
  const CircularExperience({Key key, this.dato, this.encabezado}) : super(key: key);

  final encabezado;
  final dato;

  @override
  Widget build(BuildContext context) {
    return CircularPercentIndicator(
        // Decoración del gráfico circular personalizado
        animation: true,
        animationDuration: 3000,
        circularStrokeCap: CircularStrokeCap.round,
        radius: 150.0,
        lineWidth: 15.0,
        progressColor: Colors.blue[400],
        curve: Curves.fastOutSlowIn,

        // Asignamos porcentaje del dato (Tiene que ser de tipo Double)
        percent: getPercentLevel(dato),
        header: encabezado,
        center: Text("${(getPercentLevel(dato) * 100).round()}%", style: h1()));
  }
}

class CardLanguage extends StatelessWidget {
  const CardLanguage({Key key, this.lenguaje, this.color, this.experienciaTotal, this.nuevaExperiencia}) : super(key: key);

  final String lenguaje;
  final int experienciaTotal;
  final int nuevaExperiencia;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      //Construcción del contenido
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(lenguaje, style: h2()),
          Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.stars),
                  Text(" ${getLevel(experienciaTotal)}", // Obtener el nivel de experiencia total
                      style: h3()),
                ],
              ),
              SizedBox(height: 10),
              CircularExperience(dato: experienciaTotal),
              SizedBox(height: 10),
              Text("Hoy: $nuevaExperiencia\nTotal: $experienciaTotal", style: h3()),
            ],
          ),
        ],
      ),
    );
  }
}

/*children: <Widget>[
                                CardLanguage(lenguaje: "Python", color: Colors.red[400]),
                                CardLanguage(lenguaje: "Dart", color: Colors.pink[400]),
                                CardLanguage(lenguaje: "Java", color: Colors.purple[400]),
                                CardLanguage(lenguaje: "TypeScript", color: Colors.deepPurple[400]),
                                CardLanguage(lenguaje: "XML", color: Colors.indigo[400]),
                                CardLanguage(lenguaje: "HTML", color: Colors.blue[400]),
                                CardLanguage(lenguaje: "C++", color: Colors.blueAccent[400]),
                                CardLanguage(lenguaje: "JSON", color: Colors.cyan[400]),
                                CardLanguage(lenguaje: "JavaScript", color: Colors.teal[400]),
                                CardLanguage(lenguaje: "CSS", color: Colors.green[400]),
                                CardLanguage(lenguaje: "SCSS", color: Colors.greenAccent[400]),
                                CardLanguage(lenguaje: "Docker", color: Colors.lime[400]),
                              ],*/
