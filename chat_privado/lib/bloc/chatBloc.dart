import 'dart:async';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

// TIP: No es necesario usar Maps para obtener los datos de un usuario. En este caso utilizamos clases y obtenemos sus propiedades.
class ChatModel {
  final String name;
  final int number;
  final String message;
  final String hour;
  final int newMessage;
  final String avatar;
  final String state;

  ChatModel({
    this.name,
    this.message = "",
    this.hour = "00:00",
    this.avatar = "No Avatar",
    this.newMessage = 0,
    this.state = "Conectado",
    this.number = 000,
  });
}

List<ChatModel> users = [];
String nick = "Desconocido";

class BlocChat {
  final firebase = FirebaseDatabase.instance.reference();
  StreamController _usersController = StreamController();
  Stream get getUsersController => _usersController.stream;

  // Constructor de nuevo usuario
  void addUser({
    String name = "Desconocido",
    @required int number,
    String message = "",
    String hour = "",
    int newMessage = 0,
    String state = "Desconectado",
    String avatar = "Avatar",
  }) {
    // Se suben los datos a la nube
    firebase.child(name).set({'Nombre': name, 'Número': number, "Mensaje": message, "Hora": hour, "Nuevo Mensaje": newMessage, "Estado": state, "Avatar": avatar});
  }

  void delUser(name) {
    // Eliminamos un usuario de la lista de usuarios.
    firebase.child(name).remove();
    getUsers();
  }

  void updateData({String name, String key, String value}) {
    firebase.child(name).update({key: value});
  }

  getUsers() {
    // Vaciar la lista
    print("ELIMINANDO LISTA DE USUARIOS: $users");
    users.clear();
    print("SE ELIMINÓ LA LISTA DE USUARIOS: $users");
    // Obtener los datos
    firebase.once().then((DataSnapshot snapshot) {
      if (snapshot.value != null)
        for (String username in snapshot.value.keys.toList()) {
          users.add(
            ChatModel(
                name: username,
                number: snapshot.value[username]["Número"],
                message: snapshot.value[username]["Mensaje"],
                hour: snapshot.value[username]["Hora"],
                newMessage: snapshot.value[username]["Nuevo Mensaje"],
                state: snapshot.value[username]["Estado"],
                avatar: snapshot.value[username]["Avatar"]),
          );
        }
      _usersController.sink.add(users);
    });
    return getUsersController;
  }

  void dispose() {
    _usersController.close();
  }
}
