import 'package:flutter/material.dart';

// ICONOS
Icon searchIcon = Icon(Icons.search);
Icon newMsgIcon = Icon(Icons.comment, color: whiteColor);
Icon moreOptionsIcon = Icon(Icons.more_vert);
Icon startVideocamIcon = Icon(Icons.videocam);
Icon startCallIcon = Icon(Icons.call);
Icon dialpadIcon = Icon(Icons.dialpad, size: 20);
Icon unreadMsgIcon = Icon(Icons.fiber_manual_record, color: primaryColor);
Icon checkIcon = Icon(Icons.done_all, color: secondaryColor);
Icon backIcon = Icon(Icons.arrow_back, color: whiteColor);
Icon emojiIcon = Icon(Icons.sentiment_satisfied);
Icon cameraIcon = Icon(Icons.camera_alt);
Icon sendIcon = Icon(Icons.send);
Icon contactIcon = Icon(Icons.group_add, color: whiteColor);
Icon groupIcon = Icon(Icons.group, color: whiteColor);

// COLORES
Color primaryColor = Colors.teal;
Color secondaryColor = Colors.greenAccent;
Color onlineColor = Colors.greenAccent;
Color disconnectedColor = Colors.grey;
Color bussyColor = Colors.red;
Color whiteColor = Colors.white;
