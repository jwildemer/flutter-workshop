import 'package:chat_privado/screen/Home.dart';
import 'package:chat_privado/ui/styles.dart';
import 'package:flutter/material.dart';
import 'package:chat_privado/bloc/chatBloc.dart';

class ChatScreen extends StatefulWidget {
  final String name;
  final String state;
  final String message;
  ChatScreen({Key key, this.name, this.state, this.message}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final messageInput = TextEditingController();
  BlocChat contacts = BlocChat();
  List listMessages = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: secondaryColor,
        appBar: AppBar(
          titleSpacing: 0.0,
          title: ListTile(
            title: Text(widget.name, style: TextStyle(color: whiteColor, fontWeight: FontWeight.bold)),
            subtitle: Text(widget.state, style: TextStyle(color: whiteColor)),
          ),
          leading: FlatButton(
            padding: EdgeInsets.all(0),
            child: Row(
              children: [
                backIcon,
                Expanded(
                  child: CircleAvatar(backgroundColor: secondaryColor, radius: 30),
                ),
              ],
            ),
            onPressed: () {
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
            },
          ),
          actions: [
            IconButton(icon: startVideocamIcon, onPressed: () {}),
            IconButton(icon: startCallIcon, onPressed: () {}),
            IconButton(icon: moreOptionsIcon, onPressed: () {}),
          ],
          backgroundColor: primaryColor,
        ),
        body: Center(
          child: Column(
            children: [
              Expanded(
                child: Container(
                  child: StreamBuilder(
                      stream: contacts.getUsers(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData && snapshot.data.isNotEmpty) {
                          if (listMessages.isEmpty || listMessages.last != "${snapshot.data[0].name}: ${snapshot.data[0].message}") {listMessages.add("${snapshot.data[0].name}: ${snapshot.data[0].message}");}
                          else {listMessages.add("${snapshot.data[1].name}: ${snapshot.data[1].message}");}
                          return Text("${listMessages.join("\n")}", style: Theme.of(context).textTheme.bodyText1);
                        } else {
                          return CircularProgressIndicator();
                        }
                      }),
                ),
              ),
              Row(
                children: [
                  // El widget Row quiere determinar el tamaño intrínseco de sus hijos no flexibles para saber cuánto espacio le queda para los flexibles. Sin embargo, TextField no tiene un ancho intrínseco; solo sabe cómo ajustarse al ancho completo de su contenedor principal. Intentá envolverlo en un Flexible o Expanded para decirle a la Fila que espera que TextField ocupe el espacio restante:
                  Expanded(
                    child: Card(
                        child: Row(
                      children: [
                        IconButton(icon: emojiIcon, onPressed: () {}),
                        Expanded(
                          child: TextField(
                            decoration: InputDecoration(hintText: "Escribir mensaje..."),
                            keyboardType: TextInputType.text,
                            controller: messageInput,
                            autofocus: true,
                          ),
                        ),
                        IconButton(icon: cameraIcon, onPressed: () {}),
                      ],
                    )),
                  ),
                  FloatingActionButton(
                      child: sendIcon,
                      backgroundColor: primaryColor,
                      onPressed: () {
                        if (messageInput.text.isNotEmpty) {
                          setState(() {
                            /*
                            Actualizar MI NICK con mi mensaje.
                            De esta forma cualquier usuario estaría actualizando SU mensaje con SU nick
                            */
                            contacts.updateData(name: nick, key: "Mensaje", value: messageInput.text);
                            messageInput.clear();
                          });
                        }
                      })
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
