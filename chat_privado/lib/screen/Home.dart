import 'package:chat_privado/screen/Chat.dart';
import 'package:chat_privado/screen/NuevoContacto.dart';
import 'package:chat_privado/ui/styles.dart';
import 'package:flutter/material.dart';
import 'package:chat_privado/bloc/chatBloc.dart';

///Pantalla con la lista de usuarios que hay en la sala.

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat Privado',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  BlocChat contacts = BlocChat();
  TextEditingController _nickController = TextEditingController();

  void initState() {
    //Traer los datos de Firebase al iniciar la aplicación
    _showDialog();
    super.initState();
  }

  _showDialog() async {
    // En la función de compilación initState aún no se ejecutó. Entonces este contexto está vacío.
    await Future.delayed(Duration.zero);
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Bienvenido $nick"),
          content: TextField(
            controller: _nickController,
            textInputAction: TextInputAction.go,
            keyboardType: TextInputType.name,
            decoration: InputDecoration(hintText: "Ingrese su nick"),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Aceptar"),
              onPressed: () {
                if (_nickController.text.isNotEmpty)
                  for (ChatModel u in users) {
                    if (u.name == _nickController.text) {
                      print("Ese usuario ya está en la lista");
                    } else {
                      nick = _nickController.text;
                      print("Bienvenido $nick");
                      contacts.addUser(name: nick, number: 010);
                      Navigator.of(context).pop();
                    }
                  }
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Hack Chat"),
        actions: [
          IconButton(icon: searchIcon, onPressed: () {}),
          IconButton(icon: newMsgIcon, onPressed: () {}),
          IconButton(icon: moreOptionsIcon, onPressed: () {}),
        ],
        backgroundColor: primaryColor,
      ),
      body: Center(
        child: StreamBuilder(
          //Traer los datos que se actualicen en Firebase
          stream: contacts.getUsers(),
          builder: (context, snapshot) {
            print("\n¿TIENE DATOS?: ${snapshot.hasData}\nDATOS: ${snapshot.data}\nERROR: ${snapshot.hasError}\nCONEXIÓN: ${snapshot.connectionState}");
            if (snapshot.hasData) {
              if (snapshot.data.isNotEmpty) {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        ListTile(
                          leading: CircleAvatar(radius: 30, child: Text(snapshot.data[index].name[0])),
                          title: Text("${snapshot.data[index].name}"),
                          subtitle: Row(
                            children: [
                              if (snapshot.data[index].newMessage == 0) checkIcon,
                              Text("${snapshot.data[index].message}", style: Theme.of(context).textTheme.bodyText2),
                            ],
                          ),
                          trailing: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(snapshot.data[index].hour),
                              if (snapshot.data[index].newMessage != null && snapshot.data[index].newMessage >= 1) unreadMsgIcon,
                            ],
                          ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChatScreen(
                                          name: snapshot.data[index].name,
                                          state: snapshot.data[index].state,
                                          message: snapshot.data[index].message,
                                        )));
                          },
                          onLongPress: () {
                            contacts.delUser(snapshot.data[index].name);
                          },
                        ),
                        Divider(indent: 70.0),
                      ],
                    );
                  },
                );
              } else {
                return Text("No hay contactos guardados.", style: Theme.of(context).textTheme.headline5);
              }
            } else {
              return CircularProgressIndicator();
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: primaryColor,
        child: newMsgIcon,
        onPressed: () {
          // Cuando la página a la que vamos a navegar se destruya, se actualizará ésta página.
          Navigator.push(context, MaterialPageRoute(builder: (context) => NewContactScreen()));
        },
      ),
    );
  }

  void dispose() {
    contacts.dispose();
    super.dispose();
  }
}
