import 'package:chat_privado/bloc/chatBloc.dart';
import 'package:chat_privado/screen/Home.dart';
import 'package:chat_privado/ui/styles.dart';
import 'package:flutter/material.dart';

class NewContactScreen extends StatefulWidget {
  @override
  _NewContactScreenState createState() => _NewContactScreenState();
}

class _NewContactScreenState extends State<NewContactScreen> {
  final nameInput = TextEditingController();
  final phoneInput = TextEditingController();
  int totalContacts = 0;
  BlocChat contacts = BlocChat();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColor,
          leading: BackButton(
            onPressed: () {
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
            },
          ),
          title: ListTile(
            title: Text("Contactos", style: TextStyle(color: whiteColor, fontWeight: FontWeight.bold)),
            subtitle: Text("$totalContacts contactos", style: TextStyle(color: whiteColor)),
          ),
          actions: [
            IconButton(icon: searchIcon, onPressed: () {}),
            IconButton(icon: moreOptionsIcon, onPressed: () {}),
          ],
        ),
        body: Center(
          child: Column(
            children: [
              TextField(
                decoration: InputDecoration(hintText: "Nombre de usuario"),
                controller: nameInput,
                textCapitalization: TextCapitalization.words,
              ),
              TextField(
                decoration: InputDecoration(hintText: "Teléfono"),
                controller: phoneInput,
                keyboardType: TextInputType.number,
              ),
              ListTile(
                leading: Container(width: 50, height: 50, child: contactIcon, decoration: BoxDecoration(shape: BoxShape.circle, color: primaryColor)),
                title: Text("Nuevo contacto"),
                onTap: () {
                  if (nameInput.text.isNotEmpty && phoneInput.text.isNotEmpty) {
                    setState(() {
                      contacts.addUser(name: nameInput.text, number: num.tryParse(phoneInput.text));
                      nameInput.clear();
                      phoneInput.clear();
                    });
                  }
                },
              ),
              StreamBuilder(
                stream: contacts.getUsers(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Expanded(
                      child: ListView.builder(
                        itemCount: snapshot.data.length,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (BuildContext context, int index) {
                          totalContacts = snapshot.data.length;
                          return ListTile(
                            title: Text(snapshot.data[index].name),
                            subtitle: Row(
                              children: [dialpadIcon, Text("${snapshot.data[index].number}")],
                            ),
                          );
                        },
                      ),
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void dispose() {
    nameInput.dispose();
    phoneInput.dispose();
    super.dispose();
  }
}
