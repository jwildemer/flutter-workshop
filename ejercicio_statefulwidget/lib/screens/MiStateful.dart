import 'package:flutter/material.dart';

/*
IDEAS PARA UN STATEFUL:
[] Agregar CARDS con un FAB
*/

class MiStateful extends StatefulWidget {
  @override
  // El método createState() es el encargado de manejar el estado de nuestro Widget. Por ejemplo: Cada vez que presiono un botón, este contenido hace que este estado sea notificado de que fué actualizado e internamente todo el widget es renderizado nuevamente.

  // Toda esta lógica se maneja en una clase distinta.
  _MiStatefulState createState() => _MiStatefulState();
}

class _MiStatefulState extends State<MiStateful> {
  // En esta clase extendemos de la clase State en donde le decimos que MiStateful es el Stateful Widget que va a ser el controlador de estado de este Widget

  // En es esta sección (antes del build) pondremos todo lo relacionado a funciones y variables que utilizaremos en el programa

  // VARIABLES
  //* [2] Creamos la colección y valores que vamos a utilizar y por default asignamos el tema claro
  List<String> collection = ["Flutter", "Es", "Genial"];
  String texto = "Flutter";
  int index = 0;
  ThemeData tema = ThemeData.light();
  IconData iconoTema = Icons.wb_sunny;

  // FUNCIONES
  //* [1] Creamos una función para actualizar el estado de la iteración de los textos y una función para cambiar el estado del tema actual
  void actualizarTexto() {
    setState(() {
      index = index < collection.length - 1 ? index + 1 : 0;
      texto = collection[index];
    });
  }

  void cambiarTema() {
    setState(() {
      tema = tema != ThemeData.light() ? ThemeData.light() : ThemeData.dark();
      iconoTema = iconoTema != Icons.wb_sunny ? Icons.wb_sunny : Icons.brightness_2;
    });
  }

  //* [0] Creamos la pantalla base
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: tema,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Stateful Widget [$index]"),
          actions: [
            IconButton(
              icon: Icon(iconoTema),
              onPressed: cambiarTema,
            ),
          ],
        ),
        body: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(texto, style: TextStyle(fontSize: 40)),
                RaisedButton(
                  child: Text("Actualizar"),
                  onPressed: actualizarTexto,
                ),
                Slider(
                  value: index.toDouble(),
                  min: 0.0,
                  max: collection.length.toDouble() - 1,
                  divisions: collection.length - 1,
                  label: "$texto",
                  onChanged: (double value) {
                    setState(() {
                      index = value.round();
                      texto = collection[index];
                    });
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

//* En es esta sección (después del build) pondremos todo lo relacionado a Widgets propios que utilizaremos en el programa
// WIDGETS PROPIOS
