//* Importamos async
import 'dart:async';

//* Creamos la clase BLoC
class BLoCContador {
  int _counter = 0;

  //* Creamos el StreamController
  StreamController _contadorController = StreamController();
  Stream get getStreamContador => _contadorController.stream;

  //* Movemos las funciones del HomePage a este BLoC
  void incrementar() async {
    _counter++;
		//* Agregamos el cambio con un Sink
    _contadorController.sink.add(_counter);
  }

  void decrementar() async {
    _counter--;
    _contadorController.sink.add(_counter);
  }

	//* Agregamos un dispose para que no esté llamando constantemente los datos de la aplicación
  dispose() {
    _contadorController.close();
  }
}