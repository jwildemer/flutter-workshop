import 'package:flutter/material.dart';
import '../BLoC/contador.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //* Seteamos nuestro BLoC
  BLoCContador contador = BLoCContador();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Ejercicio BLoC")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Presionaste el botón esta cantidad de veces:"),
            //* Creamos un StreamBuilder para conectar los datos del BLoC
            StreamBuilder(
              stream: contador.getStreamContador,
              builder: (context, snapshot) {
                  return Text("${snapshot.data ?? 0}", style: Theme.of(context).textTheme.headline4);
              },
            ),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: contador.decrementar,
            tooltip: "Decrementar",
            child: Icon(Icons.remove),
          ),
          SizedBox(width: 5),
          FloatingActionButton(
            onPressed: contador.incrementar,
            tooltip: "Incrementar",
            child: Icon(Icons.add),
          ),
        ],
      ),
    );
  }

  //* Agregamos un dispose para que no esté llamando constantemente los datos de la aplicación
  void dispose() {
    contador.dispose();
    super.dispose();
  }
}
