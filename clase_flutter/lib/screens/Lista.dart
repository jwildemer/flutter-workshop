import 'package:flutter/material.dart';

class MiApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Este escribe en pantalla todos los widgets que estamos creando en este StatelessWidget.

    //Imagen al azar: https://source.unsplash.com/400x400/?face

    String loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consequat lacinia metus.";

    Map<String, Map> usuarios = {
      "Patricio": {"foto": "https://i.ibb.co/n6dvtN1/Pato-Gianella.jpg", "descripción": loremIpsum},
      "Lara": {"foto": "https://i.ibb.co/gVVghmg/Lara-Gonzalez.jpg", "descripción": loremIpsum},
      "Irina": {"foto": "https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg-1024x683.jpg", "descripción": loremIpsum},
      "María": {"foto": "https://i.ibb.co/crBfkZV/Mar-a-Esquivel.jpg", "descripción": loremIpsum},
      "Julia": {"foto": "https://i.ibb.co/vmsXNNN/Julia-Melo.jpg", "descripción": loremIpsum},
      "Belén": {"foto": "https://i.ibb.co/99Q25RC/Bel-n-Cavanagh.jpg", "descripción": loremIpsum},
      "Lucila": {"foto": "https://i.ibb.co/48zDh4q/Lucila-Sarquis.jpg", "descripción": loremIpsum},
      "Rainana": {"foto": "https://i.ibb.co/mbpvrFD/Raiana-Lira.jpg", "descripción": loremIpsum}
    };

    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.menu, size: 30),
        title: Text("Mi primer AppBar"),
        actions: [
          //Lista de íconos: https://material.io/resources/icons/?style=baseline
          Padding(padding: EdgeInsets.all(8), child: Icon(Icons.search, size: 30)),
          Padding(padding: EdgeInsets.all(8), child: Icon(Icons.more_vert, size: 30)),
        ],
      ),
      body: Container(
        child: ListView.builder(
          padding: EdgeInsets.only(top: 10),
          itemCount: usuarios.length, // Si no especificamos cuantos ítems tendrá, la lista será infinita.
          itemBuilder: (context, index) {
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(radius: 27, backgroundImage: NetworkImage(usuarios.values.toList()[index]["foto"])),
                  title: Text(usuarios.keys.toList()[index]),
                  subtitle: Text(usuarios.values.toList()[index]["descripción"]),
                ),
                Divider(indent: 70.0),
              ],
            );
          },
        ),
      ),
    );
  }
}
