// Usaremos Dart:Async el cual nos permite usar Stream, Controllers y Sinks.
import 'dart:async';

class CounterBloc {
  int _contador = 0;

  /// este es el canal por donde se envia info al front end
  final _contadorController = StreamController<int>();

  /// este es el objeto por el cual el front end va a poder recibir los datos
  Stream<int> get getStreamContador => _contadorController.stream;


  /// ejemplos con eventos

  /// cuando en el onPressed de algun widget en el front end se llame a este archivo se agregan cambios al stream controller
  void incrementEvent(){
    _contador++;
    //con sink metes cosas al stream controller, y el que este escuchando el stream se le va a actualizar la info
    //si el stream builder tenia un 5 que recibio del stream no le suma el proximo numero, lo reemplaza.
    _contadorController.sink.add(_contador);//recien en esta linea el front end recibe el cambio
  }

  /// cuando en el onPressed de algun widget en el front end se llame a este archivo se agregan cambios al stream controller
  void decrementEvent(){
     _contador--;
     _contadorController.sink.add(_contador); //recien en esta linea el front end recibe el cambio
  }

  void dispose() {
    _contadorController.close();
  }
}
