abstract class CounterEvent {} //Este CounterEvent es la clase base donde todas las subsecuencias de los eventos "Incrementar" y "Disminuir" cuando sea inerte. (?)

class IncrementEvent extends CounterEvent {}

class DecrementEvent extends CounterEvent {}

