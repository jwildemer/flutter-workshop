import 'package:flutter/material.dart';
import 'package:presupuesto/bloc/PresupuestoBloc.dart';
import 'package:presupuesto/screens/AgregarEditarRequerimiento.dart';
import 'package:presupuesto/screens/Config.dart';
import 'package:presupuesto/ui/styles.dart';

//TO-DO:
/*
[*] Editar algún elemento
[*] Formato de fecha según las horas totales de los requerimientos
[*] Crear pantalla de configuración
[] Importar imagen desde URL
[] Optimizar para responsive [Pantalla en horizontal]
[] Darle estilo y animaciones [OPCIONAL]
[] Guardar los valores localmente o en Firebase [OPCIONAL]
*/

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Presupuesto',
      theme: ThemeData.dark(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  BLoC presupuesto = BLoC();
  BLoC nombreProyecto = BLoC();

  void initState() {
    nombreProyecto.getProjectName();
    presupuesto.getRequirements();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Padding(
        padding: EdgeInsets.all(15),
        child: Center(
          child: Column(
            children: <Widget>[
              // HEADER
              FlatButton(
                padding: EdgeInsets.all(0),
                child: Card(
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: StreamBuilder(
                      stream: nombreProyecto.getStreamProjectName,
                      builder: (context, streamName) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Proyecto:\n${streamName.hasData && streamName.data.isNotEmpty ? streamName.data.last : "Sin nombre"}", style: Theme.of(context).textTheme.headline3),
                            streamName.hasData ? Icon(logo, size: 100) : CircularProgressIndicator(),
                          ],
                        );
                      },
                    ),
                  ),
                ),
                onPressed: () {
                  // Navegamos a la siguiente pantalla y al volver actualiza la pantalla actual
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ConfigScreen())).then((value) => setState(() {}));
                },
              ),

              // SECCIÓN REQUERIMIENTOS
              StreamBuilder(
                stream: presupuesto.getStreamRequirements,
                builder: (context, streamRequirements) {
                  if (streamRequirements.hasData && streamRequirements.data.isNotEmpty) {
                    return Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            children: [
                              // Botón para agregar requerimientos
                              FlatButton(
                                child: Text("Requerimientos", style: Theme.of(context).textTheme.headline4),
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => AgregarEditarRequerimientoScreen())).then((value) => setState(() {}));
                                },
                              ),
                              ListView.builder(
                                shrinkWrap: true,
                                itemCount: streamRequirements.data.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Column(
                                    children: [
                                      Card(
                                        child: ListTile(
                                          leading: Icon(streamRequirements.data[index].type.icon, size: 40),
                                          title: Text(streamRequirements.data[index].name),
                                          subtitle: Text("\$${streamRequirements.data[index].cost}"),
                                          trailing: Text("${streamRequirements.data[index].hour}hs", style: Theme.of(context).textTheme.headline6),
                                          onTap: () {
                                            Navigator.push(context, MaterialPageRoute(builder: (context) => AgregarEditarRequerimientoScreen(index))).then((value) => setState(() {}));
                                          },
                                          onLongPress: () {
                                            presupuesto.delRequirement(streamRequirements.data[index]);
                                          },
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              ),
                            ],
                          ),

                          // SECCIÓN FECHAS
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text("Inicia: ${presupuesto.getCurrentDate()}", style: Theme.of(context).textTheme.headline6, textAlign: TextAlign.left),
                              Text("Entrega: ${presupuesto.getEstimatedDate()}", style: Theme.of(context).textTheme.headline6, textAlign: TextAlign.right),
                            ],
                          ),

                          // SECCIÓN PRECIO TOTAL (FOOTER)
                          Card(
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Text("\$${presupuesto.getTotalCost()}", style: Theme.of(context).textTheme.headline2, textAlign: TextAlign.center),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return ListTile(
                      leading: Icon(agregar, size: 30),
                      title: Text("Nuevo requerimiento"),
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => AgregarEditarRequerimientoScreen())).then((value) => setState(() {}));
                      },
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void dispose() {
    nombreProyecto.dispose();
    presupuesto.dispose();
    super.dispose();
  }
}
