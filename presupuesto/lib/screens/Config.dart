import 'package:flutter/material.dart';
import 'package:presupuesto/bloc/PresupuestoBloc.dart';

class ConfigScreen extends StatefulWidget {
  @override
  _ConfigScreenState createState() => _ConfigScreenState();
}

class _ConfigScreenState extends State<ConfigScreen> {
  BLoC presupuesto = BLoC();
  TextEditingController _inputController = TextEditingController();
  TextEditingController _projectNameController = TextEditingController();

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: "Configuración",
        theme: ThemeData.dark(),
        home: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            leading: BackButton(onPressed: () => Navigator.pop(context)),
            title: Text("Configuracion"),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextField(
                  controller: _projectNameController,
                  autofocus: true,
                  style: Theme.of(context).textTheme.headline5,
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(hintText: "Ingrese el nombre del proyecto"),
                  onSubmitted: (value) {
                    if (value.isNotEmpty) presupuesto.replaceProjectName(value);
                  },
                ),
                GridView.count(
                  crossAxisCount: 2,
                  shrinkWrap: true,
                  childAspectRatio: 1.5,
                  children: List.generate(
                    categoriesAndPrice.length,
                    (index) => Card(
                      child: FlatButton(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(categoriesAndPrice.keys.toList()[index], style: Theme.of(context).textTheme.headline5),
                            Text("\$${categoriesAndPrice.values.toList()[index].toInt()}", style: Theme.of(context).textTheme.headline3),
                          ],
                        ),
                        onPressed: () => showDialog(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                              title: Text(categoriesAndPrice.keys.toList()[index]),
                              content: TextField(
                                controller: _inputController,
                                textInputAction: TextInputAction.done,
                                autofocus: true,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(hintText: "Ingrese el precio por hora"),
                              ),
                              actions: [
                                FlatButton(
                                  child: Text("Aceptar"),
                                  onPressed: () {
                                    if (_inputController.text.isNotEmpty) {
                                      categoriesAndPrice[categoriesAndPrice.keys.toList()[index]] = double.parse(_inputController.text);
                                      setState(() {});
                                      _inputController.text = "";
                                      Navigator.of(context).pop();
                                    }
                                  },
                                ),
                              ],
                            ),
                        ),
                      ),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(25),
                          child: Column(
                            children: [
                              Text("Horas de trabajo disponible", style: Theme.of(context).textTheme.headline5),
                              // Puede insertarse un Slider acá para manejar las horas disponibles
                              Text("${presupuesto.hsAvailable}hs", style: Theme.of(context).textTheme.headline3),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );

  void dispose() {
    _inputController.dispose();
    _projectNameController.dispose();
    super.dispose();
  }
}
