import 'package:flutter/material.dart';
import 'package:presupuesto/bloc/PresupuestoBloc.dart';
import 'package:presupuesto/ui/styles.dart';

class AgregarEditarRequerimientoScreen extends StatefulWidget {
  AgregarEditarRequerimientoScreen([this.index]);
  final int index;

  @override
  _AgregarEditarRequerimientoScreenState createState() => _AgregarEditarRequerimientoScreenState();
}

class _AgregarEditarRequerimientoScreenState extends State<AgregarEditarRequerimientoScreen> {
  TextEditingController _titleController = TextEditingController();
  TextEditingController _hourController = TextEditingController();
  BLoC _presupuesto = BLoC();

  ItemDropList _valorElegido;

  @override
  Widget build(BuildContext context) => MaterialApp(
      theme: ThemeData.dark(),
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(onPressed: () => Navigator.pop(context)),
          title: Text(widget.index == null ? "Nuevo requerimiento" : "Editar requerimiento"),
        ),
        body: Padding(
          padding: EdgeInsets.all(13),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Card(
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        TextField(
                          controller: _titleController,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.done,
                          textCapitalization: TextCapitalization.sentences,
                          autofocus: true,
                          decoration: InputDecoration(icon: Icon(titulo, size: 30), labelText: "Ingrese un título"),
                        ),
                        TextField(
                          controller: _hourController,
                          keyboardType: TextInputType.number,
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(icon: Icon(tiempo, size: 30), labelText: "Ingrese la hora estimada"),
                        ),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(right: 16),
                              child: Icon(tipo, size: 30),
                            ),
                            Expanded(
                              child: DropdownButton(
                                value: _valorElegido,
                                hint: Text("Seleccione un tipo de requerimiento"),
                                items: tipoRequerimientos
                                    .map((tipo) => DropdownMenuItem(
                                          value: tipo,
                                          child: Row(
                                            children: [
                                              Icon(tipo.icon),
                                              SizedBox(width: 10),
                                              Text(tipo.name),
                                            ],
                                          ),
                                        ))
                                    .toList(),
                                onChanged: (value) => setState(() => _valorElegido = value),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                RaisedButton(
                  child: Text("Aceptar"),
                  onPressed: () {
                    widget.index == null
                        ? _presupuesto.addRequirement(_titleController.text, num.tryParse(_hourController.text), _valorElegido)
                        : _presupuesto.updateRequirement(widget.index, _titleController.text, num.tryParse(_hourController.text), _valorElegido);
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );

  void dispose() {
    _titleController.dispose();
    _hourController.dispose();
    super.dispose();
  }
}
