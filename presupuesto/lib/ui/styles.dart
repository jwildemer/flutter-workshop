import 'package:flutter/material.dart';

Color blanco = Colors.white;

IconData coding = Icons.code;
IconData mockup = Icons.art_track;
IconData administrative = Icons.account_box;
IconData testing = Icons.bug_report;
IconData comision = Icons.card_giftcard;
IconData tiempo = Icons.av_timer;
IconData titulo = Icons.comment;
IconData tipo = Icons.category;
IconData agregar = Icons.add;
IconData logo = Icons.supervised_user_circle;
