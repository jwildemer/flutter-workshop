// Importamos async para utilizar los Streams
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:presupuesto/ui/styles.dart';
import 'package:intl/intl.dart';

// Definimos los datos que vamos a utilizar.
List<Requirement> _requerimientos = [];
// ProjectName tiene que ser una lista para que el StreamController detecte los eventos (?)
List _projectName = ['Sin nombre'];
Map<String, double> categoriesAndPrice = {'Desarrollo': 0.00, 'Mockup': 0.00, 'Administrativo': 0.00, 'Comisión': 0.00, 'Testing': 0.00, 'Otro': 0.00};

// Creamos la clase BLoC
class BLoC {
  double cost = 0;
  int hsAvailable = 5;

  // Creamos el StreamController para el nombre del proyecto
  StreamController _projectNameController = StreamController();
  Stream get getStreamProjectName => _projectNameController.stream;

  // Creamos el StreamController para los requerimientos
  StreamController _requirementsController = StreamController();
  Stream get getStreamRequirements => _requirementsController.stream;

  // MÉTODOS
  // Estos métodos son utilizados para inicializar la aplicación con nombre de proyecto y requerimientos
  getProjectName() async {
    _projectNameController.sink.add(_projectName);
    return getStreamProjectName;
  }

  getRequirements() async {
    _requirementsController.sink.add(_requerimientos);
    return getStreamRequirements;
  }

  // Métodos para obtener fechas estimadas
  getCurrentDate() => DateFormat('dd/MM').format(DateTime.now());

  getEstimatedDate() => DateFormat('dd/MM').format(DateTime.now().add(Duration(days: getTotalDaysOfWork())));

  getTotalDaysOfWork() {
    int hsTotal = 0;
    _requerimientos.forEach((requirement) => hsTotal += requirement.hour);
    return (hsTotal / hsAvailable).round();
  }

  // Métodos para calcular y obtener precios
  calculatePrice(int hour, String type) => cost = hour * categoriesAndPrice[type];

  getTotalCost() {
    double total = 0;
    for (int item = 0; item < _requerimientos.length; item++) {
      total = total + _requerimientos[item].cost;
    }
    return total;
  }

  void replaceProjectName(String name) => _projectName[0] = name;

  // Métodos para agregar, editar y eliminar requerimientos
  void addRequirement(String name, int hour, ItemDropList type) => _requerimientos.add(
        Requirement(
          name: name,
          type: type,
          hour: hour,
          cost: calculatePrice(hour, type.name),
        ),
      );

  void updateRequirement(int index, String name, int hour, ItemDropList type) => _requerimientos[index] = Requirement(
        name: name,
        type: type,
        hour: hour,
        cost: calculatePrice(hour, type.name),
      );

  void delRequirement(index) => _requerimientos.remove(index);

  // Agregamos un dispose para que no esté llamando constantemente los datos de la aplicación
  dispose() {
    _requirementsController.close();
    _projectNameController.close();
  }
}

class ItemDropList {
  ItemDropList(this.name, this.icon);
  final String name;
  final IconData icon;
}

class Requirement {
  Requirement({this.name, this.type, this.hour, this.cost});
  String name;
  ItemDropList type;
  int hour;
  double cost;
}

//TODO: Revisar el indexado de elementos
List tipoRequerimientos = [
  ItemDropList(categoriesAndPrice.keys.toList()[0], coding),
  ItemDropList(categoriesAndPrice.keys.toList()[1], mockup),
  ItemDropList(categoriesAndPrice.keys.toList()[2], administrative),
  ItemDropList(categoriesAndPrice.keys.toList()[3], comision),
];
